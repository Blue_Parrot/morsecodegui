#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnNext_clicked();

    void on_btnAnswer_clicked();

private:
    Ui::Widget *ui;
    int lastInt;
    int score;
    int total;
    void check();
    void next();
};

#endif // WIDGET_H
