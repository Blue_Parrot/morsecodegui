#ifndef DICTIONARY_H
#define DICTIONARY_H

namespace dictionary
{
    std::string toMorse[26] =
    {
        ".-",
        "-...",
        "-.-.",
        "-..",
        ".",
        "..-.",
        "--.",
        "....",
        "..",
        ".---",
        "-.-",
        ".-..",
        "--",
        "-.",
        "---",
        ".--.",
        "--.-",
        ".-.",
        "...",
        "-",
        "..-",
        "...-",
        ".--",
        "-..-",
        "-.--",
        "--..",
    };

    char toEnglish[26] =
    {
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z'
    };

    std::string mnemonics[26] =
    {
        "Archery",
        "Banjo",
        "Candy",
        "Dog",
        "Eye",
        "Firetruck",
        "Giraffe",
        "Hippo",
        "Insect",
        "Jet",
        "Kite",
        "Laboratory",
        "Mustache",
        "Net",
        "Orchestra",
        "Paddle",
        "Quarterback",
        "Robot",
        "Submarine",
        "Tape",
        "Unicorn",
        "Vacuum",
        "Wand",
        "X-ray",
        "Yard",
        "Zebra"
    };
}

#endif // DICTIONARY_H
