#include "widget.h"
#include "ui_widget.h"
#include "dictionary.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    setWindowFlags(Qt::Widget | Qt::MSWindowsFixedSizeDialogHint);
    ui->setupUi(this);

    score = total = 0;

    QFont fontStyle("Consolas", 18, QFont::Normal);
    ui->outputLabel->setFont(fontStyle);
    ui->outputLabel->setText("Select an Option<br>and Click Next");
}

Widget::~Widget()
{
    delete ui;
}

void Widget::check()
{
    std::string input = ui->lineInput->text().toStdString();
    bool isCorrect = true;

    // morse => english
    if(ui->optSelect->currentIndex() == 1)
    {
        if(input.length() != 1)
            isCorrect = false;
        if(tolower(input[0]) != tolower(dictionary::toEnglish[lastInt]))
            isCorrect = false;
    }
    // english => morse
    else if(ui->optSelect->currentIndex() == 2)
    {
        if(input.length() > 4 || input.length() == 0)
            isCorrect = false;
        else
        {
            if(input.length() == dictionary::toMorse[lastInt].length())
            {
                for(int i = 0; i < input.length(); i++)
                {
                    if(input[i] != dictionary::toMorse[lastInt][i])
                        isCorrect = false;
                }
            }
            else
                isCorrect = false;
        }
    }

    if(isCorrect && ui->optSelect->currentIndex() != 0)
    {
        ui->resultLabel->setText("Correct");
        score++;
    }
    else if(!isCorrect && ui->optSelect->currentIndex() == 1)
    {
        QString ansOut = "Answer: ";
        ansOut.append(QChar(dictionary::toEnglish[lastInt]));
        ui->resultLabel->setText(ansOut);
    }
    else if(!isCorrect && ui->optSelect->currentIndex() == 2)
    {
        QString ansOut = QString::fromStdString(dictionary::toMorse[lastInt]);
        ansOut.prepend("Answer: ");
        ui->resultLabel->setText(ansOut);
    }

    if(ui->optSelect->currentIndex() != 0)
        total++;

    QString scoreOut = "Score: ";
    scoreOut.append(QString::number(score));
    scoreOut.append('/');
    scoreOut.append(QString::number(total));
    ui->scoreLabel->setText(scoreOut);
}

void Widget::next()
{
    srand(time(NULL));
    std::string temp;
    int x = 0;

    QFont fontStyle("Consolas", 48, QFont::Normal);
    ui->outputLabel->setFont(fontStyle);

    do
    {
        x = rand() % 26;
    } while(x == lastInt);

    lastInt = x;

    // morse => english
    if(ui->optSelect->currentIndex() == 1)
        temp += dictionary::toMorse[x];
    // english => morse
    else if(ui->optSelect->currentIndex() == 2)
        temp += dictionary::toEnglish[x];

    if(ui->optSelect->currentIndex() != 0)
    {
        ui->outputLabel->setText(QString::fromStdString(temp));
        ui->resultLabel->setText("");
    }
    else
    {
        QFont fontStyle("Consolas", 18, QFont::Normal);
        ui->outputLabel->setFont(fontStyle);
        ui->outputLabel->setText("Select an Option<br>and Click Next");
    }
}

void Widget::on_btnNext_clicked()
{
    if(ui->btnNext->text() == "Next")
    {
        next();
        ui->btnNext->setText("Check");
        ui->btnAnswer->setEnabled(true);
    }
    else if(ui->btnNext->text() == "Check")
    {
        check();
        ui->btnNext->setText("Next");
        ui->btnAnswer->setEnabled(false);
    }
}

void Widget::on_btnAnswer_clicked()
{
    QString ansOut;
    if(ui->optSelect->currentIndex() == 1)
        ansOut = QChar(dictionary::toEnglish[lastInt]);
    else if(ui->optSelect->currentIndex() == 2)
        ansOut = QString::fromStdString(dictionary::toMorse[lastInt]);

    ansOut.prepend("Answer: ");
    ui->btnAnswer->setEnabled(false);
    ui->btnNext->setText("Next");
    ui->resultLabel->setText(ansOut);
}

/*
TODO:
    - may want to have a longer history for lastInt
*/
